"""build.py collect the markdown files in content/ and build the pages
in fr/ en/ etc...

(c) The ParisAgreement.fr dev team. GPLv3+
"""

import glob
import os
import logging
import markdown as md
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

with open('template.html.in', 'r') as file:
    TEMPLATE = file.read()

with open('index.html.in', 'r') as file:
    TEMPLATE_IDX = file.read()

with open('table.html.in', 'r') as file:
    TEMPLATE_TABLE = file.read()

SCRIPTS = """
      <!-- Piwik -->
      <script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
	var u="//paris-agreement.fr/frequentation/";
	_paq.push(['setTrackerUrl', u+'piwik.php']);
	_paq.push(['setSiteId', 1]);
	var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	})();
      </script>
      <noscript><p><img src="//paris-agreement.fr/frequentation/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
      <!-- End Piwik Code -->      
"""

TRADUCTIONS = {
    'path':{'fr':'', 'en':''},
    'menu_home':{'fr':'Accueil', 'en':'Home'},
    'menu_ratification':{'fr':'Ratification ?', 'en':'Ratification?'},
    'menu_agreement':{'fr':'Accord de Paris ?', 'en':'Agreement?'},
    'menu_involved':{'fr':'Participer', 'en':'Get Involved'},
    'menu_sources':{'fr':'Sources et Resources', 'en' :'Sources and Resources'},
    'menu_about':{'fr':'À propos', 'en':'About'},
    'menu_index':{'fr':'Sommaire', 'en':'Index'},
    'menu_table':{'fr':'Données', 'en':'Data'},
    'link_table':{'fr':'/table.fr.html', 'en':'/table.en.html'},
    'link_index':{'fr':'/fr/index.html', 'en':'/en/index.html'},
    'link_home':{'fr':'/index.fr.html', 'en':'/index.en.html'},
    'link_ratification':{'fr':'/fr/ratification.html', 'en':'/en/ratification.html'},
    'link_agreement':{'fr':'/fr/accord.html', 'en':'/en/agreement.html'},
    'link_involved':{'fr':'/fr/participer.html', 'en':'/en/get-involved.html'},
    'link_sources':{'fr':'/fr/sources.html', 'en':'/en/sources.html'},
    'link_about':{'fr':'/fr/about.html', 'en':'/en/about.html'},
    'scripts':{'fr':SCRIPTS, 'en':SCRIPTS},
}

FIELDS = {}
FIELDS["en"] = {x:TRADUCTIONS[x]['en'] if 'en' in TRADUCTIONS[x] else x for x in TRADUCTIONS}
FIELDS["fr"] = {x:TRADUCTIONS[x]['fr'] if 'fr' in TRADUCTIONS[x] else x for x in TRADUCTIONS}

TRADUCTIONS = {
    'status':{'fr':'Statut', 'en':"Status"},
    'process':{'fr':'Processus de Ratification', 'en':"Ratification Process"},
    'avancement_title':{'fr':'Où en est-on ?', 'en':"How is it going?"},
    'panel_placeholder':{'fr':'Cliquez sur un pays pour plus d\'informations',
                         'en':"Click on a country to get more information"},
    'what_title':{'fr':'De quoi parle-t-on?', 'en':"What is it about?"},
    'proportion_text':{'fr':'Proportion des émissions globales couvertes par l\'accord',
                       'en':'Percentage of global emissions covered by the agreement'},
    'danger_text':{'fr':'Ce site est en construction.',
                   'en':'This website is under construction. Stay tuned!'},
    'what_text':{'fr':"""En décembre 2015, toutes les parties de la COP21 se sont mises d'accord sur ce
que l'on appelle l'accord de Paris. Il semblait alors que nos
gouvernements allaient dans la bon sens ! En avons-nous fini avec
le changement climatique ? Probablement pas, ce n'est que le début: il
a fallu 8 ans pour ratifier le protocole de Kyoto. Faisons en sorte
que cela ne prenne pas autant de temps pour l'accord de Paris. <br/>L'accord est ouvert à signature depuis le <em>22 avril 2016</em>. <br/> Cette carte donne les clés pour suivre la ratification de l'accord.""",
                 'en':"""In December 2015 all the COP21 parties agreed on the so-called "Paris
Agreement". At that time it seemed our governments were heading to
a more liveable story. Is it the end of the journey? Certainly not, it is
only the beginning. It took eight years to have the Kyoto
protocol come into force. Let's ensure that it will not take that long for the Paris
Agreement. <br/>The Paris Agreement signature process began on <em>22 April 2016</em>.<br/>This map provides insights into the processes of ratification of the Agreement."""},
    "title_ratification":{'fr':'Ratification de l\'accord de Paris',
                          'en':'Paris Agreement Ratification'},
    "number_of_parties_text":{'fr':"Nombre de parties ayant ratifié l'accord :",
                              'en':"Number of parties having ratified the agreement:"},
    "number_of_signatures_text":{'fr' : " Parties l'ayant signé :",
                                 'en' : " Number of signatures:"},
    'lang':{'fr':'fr', 'en':'en'},
    'page_description':{'en':"Paris"},
    'site_title':{'en':"Paris Agreement", 'fr':"Accord de Paris"},
    'bartxt_ratified':{'en':"Ratified", 'fr':"Ratifié"},
    'bartxt_ratifying':{'en':"Signed", 'fr':"Signé"},
    'bartxt_danger':{'en':"Danger", 'fr':"Danger"},

}

FIELDS_INDEX = {}
FIELDS_INDEX["en"] = {x:TRADUCTIONS[x]['en'] if 'en' in TRADUCTIONS[x] else x for x in TRADUCTIONS}
FIELDS_INDEX["fr"] = {x:TRADUCTIONS[x]['fr'] if 'fr' in TRADUCTIONS[x] else x for x in TRADUCTIONS}

if __name__ == '__main__':
    pages = []

    for lang in FIELDS:
        if not os.path.exists(lang):
            os.mkdir(lang)
            logging.info('Creating: {}/'.format(lang))


    for fi in glob.glob("content/*.md"):
        try:
            name, lang, _ = os.path.basename(fi).split(".")
        except ValueError as ex:
            logging.warning('Bad filename: {} ({})'.format(fi, ex))

        title = name#.capitalize()
        title='{}{}'.format(title[0].upper(), title[1:])
        outfi = os.path.join(lang, name+".html")
        with open(fi, 'r') as infile:
            with open(outfi, 'w') as outfile:
                outfile.write(TEMPLATE.format(content=md.markdown(infile.read()), title=title, **FIELDS[lang]))
                logging.info('Parsed {} -> {}'.format(fi, outfi))
        pages.append((lang, title, outfi))

    index = {}
    list_pages = '\n'.join(['<li><a href="/{2}">[{0}] {1}</a></li>'.format(lang, name, fi) for lang, name, fi in sorted(pages)])
    index['en'] = '''<h1>Site Index</h1><ul>'''+list_pages+'</ul>'
    index['fr'] = '''<h1>Plan du site</h1><ul>'''+list_pages+'</ul>'
    logging.info('Parsed & Indexed {} pages'.format(len(pages)))

    for lang, fields in FIELDS.items():
        with open(os.path.join(lang, "index.html"), 'w') as file:
            file.write(TEMPLATE.format(content=index[lang], title="Index", **FIELDS[lang]))
            logging.info('template.html.in -> /{}/index.html'.format(lang))

        with open("index.{}.html".format(lang), 'w') as file:
            file.write(TEMPLATE_IDX.format(title="Index", **(dict(FIELDS[lang], **FIELDS_INDEX[lang]))))
            logging.info('index.html.in -> index.{}.html'.format(lang))

        with open("table.{}.html".format(lang), 'w') as file:
            file.write(TEMPLATE_TABLE.format(title="Table", **(dict(FIELDS[lang], **FIELDS_INDEX[lang]))))
            logging.info('table.html.in -> table.{}.html'.format(lang))


    if not os.path.exists('index.html'):
        logging.info('Linking (symbolic) index.html to index.en.html')
        os.symlink("index.en.html", 'index.html')
