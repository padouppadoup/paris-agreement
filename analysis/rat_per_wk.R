## Load libs
library(ISOweek)

## Preprocess 
tt <- read.csv("rat.csv", header=FALSE) ## Load data
names(tt) <- c("country", "signature", "ratification")
tt[,3] <- gsub(" ", "", tt[,3]) ## Remove unwanted characters 
tt[,2] <- gsub(" ", "", tt[,2])
tt[,1] <- gsub(" ", "", tt[,1])

lct <- Sys.getlocale("LC_TIME"); Sys.setlocale("LC_TIME", "C") ## Change the locale so that we can process dates properly

## Process!
tt$date.sign <- as.Date(tt[,2], "%d %b %Y")
tt$date.ratif <- as.Date(tt[,3], "%d %b %Y")
tt$week.sign <- paste(substr(date2ISOweek(tt$date.sign), 1,8),"01", sep="-")
tt$week.ratif <- paste(substr(date2ISOweek(tt$date.ratif), 1,8),"01", sep="-")

## Plot!
pdf("sign-ratif-per-week.pdf")

par(mfrow=c(2,1))
plot(table(tt$week.sign), main="Number of signatures per week", xlab="Time of year", ylab="number of signatures")
plot(table(subset(tt, week.ratif != "NA-01")$week.ratif), main="Number of ratifications per week", xlab="Time of year", ylab="number of ratifications")

dev.off()


## Restore the initial locale
Sys.setlocale("LC_TIME", lct) # thanks to http://stackoverflow.com/questions/15566875/as-date-returning-na-in-r
