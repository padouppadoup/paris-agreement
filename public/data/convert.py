# -*-coding:utf-8 -*-
# Methods to generate the GeoJSON file from several datasets
# Mar. 2016, GPLv3+
# The Paris Agreement project

## import libraries
from __future__ import print_function
import sys
import json

import pandas
import numpy as np

LANG_AVAILABLE = ('fr', 'en')

TEXTGROUPS = {
    'EMPTY':{'en':'',"fr":''},
    "EU": {"en": 'The European Union member states have a <a href="/en/UE.html">specific ratification procedure</a>.',
           "fr": 'Les États membres de l\'Union Européenne ont une <a href="/fr/UE.html">procédure de ratification particulière</a>.'}
}

def addfeatures(features, lang):
    """Add features of a CSV file to the JSON"""
    data = pandas.read_csv('./data.csv')
    geo = json.loads(open("./un_member_states_lite.geojson", 'r').read())

    # Add a text according to the group.
    data['group'].fillna('EMPTY', inplace=True)
    
    data['details.'+lang]=[str(x) if not str(x)=='nan' else '' for x in data['details.'+lang]] #.fillna('', inplace=True)
    data['details.'+lang] = [detail+TEXTGROUPS[grp][lang]
                             for detail, grp
                             in zip(data['details.'+lang], data['group'])]

    # Merge the data.csv into the geojson file.
    fields = dict([(code, feat) for code, feat in data.set_index('code')[features].iterrows()])
    for (i, country) in enumerate(geo["features"]):
        try:
            geo['features'][i]['properties'].update(fields[country['properties']['iso_a2']])
        except Exception as ex:
            print("Error merging {}/{}: {}".format(country['properties']['name'],i, ex), file=sys.stderr)
            print("Offending geo:\n {}\n ".format(country), file=sys.stderr)
    return geo

if __name__ == "__main__" :
    if len(sys.argv) == 2 and sys.argv[1] in LANG_AVAILABLE:
        LANG = sys.argv[1]
        print('Merging for lang: '+LANG, file=sys.stderr)
        CHAMPS = ['code_ratification',
                  'code_status',
                  'emissions',
                  'year',
                  'country',
                  'INDC',
                  'group',
                  'details.'+LANG]
        print(json.dumps(addfeatures(CHAMPS, LANG)).replace('NaN', 'null'))
    else:
        print('Usage: convert.py [{0}] > data.[{0}].json'.format('|'.join(LANG_AVAILABLE)))
