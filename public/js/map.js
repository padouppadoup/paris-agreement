// map.js - Display the map.
// This file is part of the paris agreement project.
// Copyright 2016 Maxime Woringer & Guilhem Doulcier, Licence GNU GPL3+

// Require leaflet and the variable /!\

// global Namespace
var map, geojson, info, legend, statesData, lang

//////////////
//// DATA ////
//////////////

var TRADUCTION = {ratif1: {en: 'Individual Chief executive or cabinet decision',
			   fr: 'Décision du pouvoir éxecutif'},
		  ratif15: {en: 'Rule or tradition of informing legislative body of signed treaties',
			    fr: 'Règle ou tradition d\'information du corps législatif'},
		  ratif2: {en: 'Majority consent of one legislative body of signed treaties',
			   fr: 'Vote Majoritaire d\'un corps legislatif'},
		  ratif3: {en: 'Super-majority in one body or majority in two separate legislative bodies',
			   fr: 'Vote à la majorité qualifiée d\'un corps législatif ou majorité dans deux corps législatifs différents'},
		  ratif4: {en: 'National plebiscite',
			   fr: 'Référendum national'},
		  unknown: {en: 'Unknown',
			    fr:'Inconnu'},
		  rat_unsigned: {en: 'Agreement not signed',
				 fr : 'Accord non signé'},
		  rat_nonUNFCCC: {en: 'Not part of the UNFCCC',
				  fr : 'Non membre de la CCNUCC'},
		  rat_signed: {en: 'Agreement signed',
			       fr : 'Accord signé'},
		  rat_ratified: {en:'Agreement ratified',
				 fr:'Accord ratifié'},
		  rat_ongoing: {en:'Legislative process initiated',
				fr:'Processus législatif enclenché'},
		  rat_danger: {en:'Somehow it\'s going poorly',
			       fr:'Il y a un truc qui va pas'},
		  hover_ratified: {en:' parties have ratified the agreement.',
				   fr:' parties ont ratifié l\'accord'},
		  hover_ratifying: {en:' parties deposited their ratification instruments.',
				   fr:' parties ont déposé leurs instruments de ratification'},
		  hover_danger:  {en:' parties are doing something weird',
				  fr:' parties font n\'importe quoi' },
		  hover_proportion:  {en:' of emissions are covered by the agreement.',
				      fr:' des émissions sont couvertes par l\'accord' },
		  layer_emissions: {en: 'Emissions', fr:'Émissions' },
		  layer_procedure: {en: 'Procedure', fr:'Procédure'},
		  layer_status: {en: 'Status', fr:'Statut'},
		  hover_prompt: {en: 'Hover a state for ratification status',
				 fr: 'Survolez un état pour suivre l\'avancement de la ratification'},
		  legend_procedure_title: {en:'Ratification procedure', fr:'Procédure de ratification'},
		  legend_status_title: {en:'Ratification status', fr: 'Avancement de la ratification'},
		  legend_status_emissions: {en:'Total emissions (Mt CO<sub>2</sub>-eq)',
					    fr:'Émissions totales (Mt CO<sub>2</sub>-eq)'},
		  map_attribution: {en: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
				    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
				    fr: 'Fond de carte par les contributeurs &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>, ' +
				    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				    'Images © <a href="http://mapbox.com">Mapbox</a>'},
		  procedure_attribution: {en:'Ratification procedure &copy; <a href="http://scholar.harvard.edu/bsimmons/">Beth Simmons</a>',
					  fr:'Procédure de ratification &copy; <a href="http://scholar.harvard.edu/bsimmons/">Beth Simmons</a>'},
		  side_title: {en: 'Ratification information', fr:'État de la ratification'},
		  side_procedure: {en:'Procedure', fr:'Procédure'},
		  side_status: {en:'Status', fr:'Statut'},
		  see_legend: {en:'(see legend for color code)', fr:'(voir légende pour le code couleur)'},
		  side_hover_prompt: {en: 'Hover over a state', fr:'Survolez un état'},
		  learn_more: {en: 'Learn more', fr:'En savoir plus'}
		 }

// Ratification codes from http://scholar.harvard.edu/bsimmons/files/APP_3.2_Ratification_rules.pdf
var ratification_codes = {1:{name:TRADUCTION['ratif1'][lang],
			     color:'#66c2a5'},
			  1.5: {name:TRADUCTION['ratif15'][lang],
				color:'#fc8d62'},
			  2: {name:TRADUCTION['ratif2'][lang],
			      color:'#8da0cb'},
			  3: {name:TRADUCTION['ratif3'][lang],
			      color:'#e78ac3'},
			  4: {name:TRADUCTION['ratif4'][lang],
			      color:'#a6d854'},
			  'default': {name:TRADUCTION['unknown'][lang],
				      color:'#f2f2f2'}
			 }



// Ratification status
var ratification_status = {'default': {name: TRADUCTION['rat_unsigned'][lang],
				       color: '#FFA500'},
                           signed: {name: TRADUCTION['rat_signed'][lang],
				      color: '#FFEDA0'},
			   ongoing:  {name: TRADUCTION['rat_ongoing'][lang],
				      color: '#94d6f7'},
			   ratified: {name: TRADUCTION['rat_ratified'][lang],
				      color: '#5cb85c'},
			   danger: {name: TRADUCTION['rat_danger'][lang],
				    color:'#d9534f'},
			   'nonUNFCCC': {name: TRADUCTION['rat_nonUNFCCC'][lang],
				       color:'#f2f2f2'}
			   }

var emissions_values = [{value: 'NA', color: '#f2f2f2'},
			{value: 1000, color: '#800026'},
			{value: 500, color:  '#BD0026'},
			{value: 250, color:  '#E31A1C'},
			{value: 100, color:  '#FC4E2A'},
			{value: 50, color:   '#FD8D3C'},
			{value: 25, color:   '#FEB24C'},
			{value: 5, color:   '#FED976'}
		       ]
var tot_emissions = 0.
////////////////
/// BARS ////
////////////////

var set_bars = function(parties, signed, ratified, ratifying, danger, emissions){
    // Set the length and text of the progress bars.
    //
    // Args:
    //   parties, ratified, ratifying and dangers are integers (a number of parties).
    //   emissions is a float (%).
    $('#bar_ratified').css({width:(ratified/parties)*100+'%'})
    $('#bar_ratified').attr('title', ratified+TRADUCTION['hover_ratified'][lang])
    $('#number_ratified').html(ratified)
    $('#number_ratified_2').html(ratified+'/'+parties)
    $('#number_signed_2').html(signed+'/'+parties)
    $('#bar_ratifying').css({width:(ratifying/parties)*100+'%'})
    $('#bar_ratifying').attr('title', ratifying+TRADUCTION['hover_ratifying'][lang])
    $('#number_ratifying').html(signed)
    $('#bar_danger').css({width:(danger/parties)*100+'%'})
    $('#number_danger').html(danger)
    $('#bar_danger').attr('title',danger+TRADUCTION['hover_danger'][lang])
    $('#bar_emissions').css({width:emissions+'%'})
    $('#number_emissions').html(emissions.toFixed(4)+'%')
    $('#bar_emissions').attr('title',emissions+TRADUCTION['hover_proportion'][lang])
}


var compute_proportions = function(data){
    var uesigned = 1 // EU signed
    var ueratified = 1 // EU ratified
    var signed = 0+uesigned
    var ratified = 0+ueratified
    var ratifying = 0
    var danger = 0
    tot_emissions = 0.
    var covered_emissions = 0

    for (var party in data.features){
	data.features[party].properties.code_status
	tot_emissions += parseFloat(data.features[party].properties.emissions ? data.features[party].properties.emissions : 0)
	switch(data.features[party].properties.code_status){
	case 'signed':
	    ratifying++
	    signed ++
	    break
	case 'ongoing':
	    signed ++
	    break
	case 'ratified':
	    ratified++
	    signed++
	    covered_emissions += data.features[party].properties.emissions ? data.features[party].properties.emissions  : 0
	    break
	case 'danger':
	    danger++
	    break
	default:
	    //	    console.log('No info for '+ data.features[party].properties.name)
	}
    }
    //set_bars(data.features.length,ratified,ratifying,danger, covered_emissions/tot_emissions)
    set_bars(197,signed, ratified,ratifying,danger, covered_emissions/tot_emissions*100)
}

////////////////
/// LEGENDS ////
////////////////

var labels = []
for (var code in ratification_codes) {
    labels.push('<span style="background:' + ratification_codes[code].color + '">&nbsp;&nbsp;</span>&nbsp;' + ratification_codes[code].name)
}
var procedure_legend = '<h4>'+TRADUCTION['legend_procedure_title'][lang]+'</h4>'+labels.join('<br>')

labels = []
for (code in ratification_status) {
    labels.push('<span style="background:' + ratification_status[code].color + '">&nbsp;&nbsp;</span>&nbsp;' + ratification_status[code].name )
}
var status_legend = '<h4>'+TRADUCTION['legend_status_title'][lang]+'</h4>'+labels.join('<br>')

labels = ['<span style="background:' + emissions_values[0].color + '">&nbsp;&nbsp;</span>&nbsp;'+emissions_values[0].value]

var grades = []
for (var i=(emissions_values.length-1); i > 0 ;i--) {
    grades.push(emissions_values[i].value)
}

for (i = 0; i < grades.length; i++) {
    var from = grades[i]
    var to = grades[i+1]

    labels.push(
	'<span style="background:' + getColor(from + 1) + '">&nbsp;&nbsp;</span> ' +
	    from + (to ? '&ndash;' + to : '+'))
}

var emissions_legend = '<h4>'+TRADUCTION['legend_status_emissions'][lang]+'</h4>'+labels.join('<br>')

///////////////////
/// REDRAW THE MAP/
///////////////////
function getColor(d) {
    // get color depending on population density value
    for (var i = 0; i < emissions_values.length; i++) {
	if (d > emissions_values[i].value) {
	    return emissions_values[i].color
	}
    }
    return '#f2f2f2'
}

function getNumber(d) {
    // Return a number or NA
    var out = (isNaN(d) ? 'Unknown' : d)
    return out
}

function redraw_map(view){
    /// This function is called whenever you want to change the information displayed on the map.
    var code
    switch(view){
    case 'procedure':
	geojson.eachLayer(function (d){
	    code = ratification_codes[d.feature.properties.code_ratification] || ratification_codes['default']
	    d.setStyle({fillColor:code.color})})
	legend.getContainer().innerHTML = procedure_legend
	break
    default:
    case 'status':
	geojson.eachLayer(function (d){
	    code = ratification_status[d.feature.properties.code_status] || ratification_status['default']
	    d.setStyle({fillColor:code.color})})
	legend.getContainer().innerHTML = status_legend
	break
    case 'emission':
	geojson.eachLayer(function (d){
	    d.setStyle({fillColor:getColor(d.feature.properties.emissions)})})
	legend.getContainer().innerHTML = emissions_legend
	break
    }
}


///////////////////
/// "RESPONSIVE"
///////////////////


var set_sizes = function(){
    // The map should take all the height
    $('#map-canvas').height($(window).height()-50)

    // On big screens, the left column should take all the height too.
    if ($(window).width() > 992) {
	$('#left').height($(window).height()-50)
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// MAIN ///
///////////////////////////////////////////////////////////////////////////////////////////////

var main = function(){
    // Set div height
    set_sizes()
    $( window ).resize(function() {
	    set_sizes()
    })

    // Setup Leaflet.
    map = L.map('map-canvas').setView([40, 15], 2)

    // Add Countries and city names through mapbox API.
    var mpbox_layer = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token='
    var mpbox_token = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw'
    L.tileLayer(mpbox_layer+mpbox_token, {
	maxZoom: 18,
	attribution: TRADUCTION['map_attribution'][lang],
	id: 'mapbox.light',
	tap: false
    }).addTo(map)

    $("#what h3").on('click', function(){$("#what p").toggle(); $("#what p").is(':visible') ?  $("#what_plus").html("-") :  $("#what_plus").html("+")})

    // Add attribution for the ratification procedure.
    map.attributionControl.addAttribution(TRADUCTION['procedure_attribution'][lang])

    //////////////////////////////////////
    /// Buttons to switch views
    //////////////////////////////////////
    var control_view = L.control()

    // Add the info div with class info (called once).
    control_view.onAdd = function () {
	this._div = L.DomUtil.create('div', 'info')
	this._div.innerHTML = '<button type="button" class="btn btn-default" onclick="redraw_map(\'status\');">'+   TRADUCTION['layer_status'][lang]+'</button> <button type="button" class="btn btn-default" onclick="redraw_map(\'procedure\');">'+TRADUCTION['layer_procedure'][lang]+'</button> <button type="button" class="btn btn-default"  onclick="redraw_map(\'emission\');">'+TRADUCTION['layer_emissions'][lang]+'</button>'
	return this._div
    }
    control_view.addTo(map)

    //////////////////////////////////////////
    // Informations on HOVER
    /////////////////////////////////////////
    info = L.control()

    // Add the info div with class info (called once).
    info.onAdd = function () {
	this._div = L.DomUtil.create('div', 'info')
	this.update()
	this._div.innerHTML = TRADUCTION['hover_prompt'][lang]
	return this._div
    }


    // When a country is clicked, update the info div.
    info.update = function (props) {
	this._div.innerHTML =
	    (props ?  '<h4>'+props.name+'</h4>' + (ratification_status[props.code_status] || ratification_status['default']).name +'<br/>'+ getNumber(props.emissions) + ' Mt CO<sub>2</sub>-eq (' +
	     (props.emissions/tot_emissions*100.).toFixed(2)+ '%, ' +
	     props.year+')<br/>'
	     : TRADUCTION['side_hover_prompt'][lang])
    }
    info.addTo(map)
	

    //////////////////////////////////////////
    // Draw shape and apply style to countries.
    /////////////////////////////////////////

    function highlightFeature(e) {
	// When hovering
	var layer = e.target

	layer.setStyle({
	    weight: 3, // size of the contour
	    color: '#666', // color of the line contour
	    dashArray: '',
	    fillOpacity: 0.7
	})

	if (!L.Browser.ie && !L.Browser.opera) {
	    layer.bringToFront()
	}

	info.update(layer.feature.properties)
    }

    function resetHighlight(e) {
	var layer = e.target

	layer.setStyle({
	    weight: 2,
	    color: 'white',
	    dashArray: '3',
	    fillOpacity: 0.7
	})

	info.update()
    }


    function zoomToFeature(e) {
	//map.fitBounds(e.target.getBounds()) // Zoom on the country
	var props = e.target.feature.properties
	
	// Hide the about section on the left:
	$('#what p').hide()
	$('#what_plus').html('+')
	
	// Update the country info
	$('#country-info').html('<h4>'+props.name +'</h4>' +
	    (e ? '<b>'+TRADUCTION['side_title'][lang]+'</b><br />'+TRADUCTION['side_status'][lang]+': ' +
	     (ratification_status[props.code_status] || ratification_status['default']).name +
	     '<br/>'+TRADUCTION['side_procedure'][lang]+': ' + '<span style="background:' +
	     (ratification_codes[props.code_ratification] || ratification_codes['default']).color
	     + '">&nbsp;&nbsp;</span> ' +
	     (ratification_codes[props.code_ratification] || ratification_codes['default']).name
	     + '<br/>'+TRADUCTION['layer_emissions'][lang]+': ' +
	     + getNumber(props.emissions) + ' Mt CO<sub>2</sub>-eq (' +
	     (props.emissions/tot_emissions*100.).toFixed(2)+ '%, '+ props.year +')<br/><br/>'+
	     '<b>'+TRADUCTION['learn_more'][lang]+'</b><br/>' +
	     '<a href="'+props.INDC+'">INDC</a><br/>' + 
	     (props['details.'+lang] ? props['details.'+lang]+'<br/>' : '')
	     :  TRADUCTION['side_hover_prompt'][lang]))
    }

    geojson = L.geoJson(statesData, {
	style: function () {
	    return {
		weight: 2,
		opacity: 1,
		color: 'white',
		dashArray: '3',
		fillOpacity: 0.7
	    }},
	onEachFeature: function (feature, layer) {
	    layer.on({
		mouseover: highlightFeature,
		mouseout: resetHighlight,
		click: zoomToFeature
	    })
	}
    }).addTo(map)

    //////////////////////////////////////////
    // Draw a legend.
    /////////////////////////////////////////
    legend = L.control({position: 'bottomright'})
    legend.onAdd = function () {
	this._div = L.DomUtil.create('div', 'info legend')
	return this._div
    }
    legend.addTo(map)

    //////////////////////////////////////////
    // Start with the default view
    /////////////////////////////////////////
    redraw_map()
    compute_proportions(statesData)
}


// When the document is ready, load the json and execute main
$(document).ready(function() {
    $.getJSON('/data/data.'+lang+'.json')
	.done(function(data){
	    statesData = data
	    main()
	})
	.fail(function(e){console.log('Failed loading data')
			  console.log(e)
			  main()})
})
