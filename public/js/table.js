// table.js
// This file is part of the paris agreement project.
// Copyright 2016 Maxime Woringer & Guilhem Doulcier, Licence GNU GPL3+

// Require d3.js

var statesData, lang

var TRADUCTION = {
    rat_unsigned: {en: 'Agreement not signed',
		   fr : 'Accord non signé'},
    rat_nonUNFCCC: {en: 'Not part of the UNFCCC',
		    fr : 'Non membre de la CCNUCC'},
    rat_signed: {en: 'Agreement signed',
		 fr : 'Accord signé'},
    rat_ratified: {en:'Agreement ratified',
		   fr:'Accord ratifié'},
    rat_ongoing: {en:'Legislative process initiated',
		  fr:'Processus législatif enclenché'},
    rat_danger: {en:'Somehow it\'s going poorly',
		 fr:'Il y a un truc qui va pas'}
}

var ratification_status = {'default': {name: TRADUCTION['rat_unsigned'][lang],
				       color: '#FFA500'},
                           signed: {name: TRADUCTION['rat_signed'][lang],
				      color: '#FFEDA0'},
			   ongoing:  {name: TRADUCTION['rat_ongoing'][lang],
				      color: '#d9edf7'},
			   ratified: {name: TRADUCTION['rat_ratified'][lang],
				      color: '#5cb85c'},
			   danger: {name: TRADUCTION['rat_danger'][lang],
				    color:'#d9534f'},
			   'nonUNFCCC': {name: TRADUCTION['rat_nonUNFCCC'][lang],
				       color:'#f2f2f2'}
			   }


// When the document is ready, load the json and execute main
$(document).ready(function() {
    $.getJSON('/data/data.'+lang+'.json')
	.done(function(data){
	    statesData = data
	    main()
	})
	.fail(function(e){console.log('Failed loading data')
			  console.log(e)
			  main()})
})

var order = {'name':1,'emissions':1,'code_status':1,'details.fr':1,'details.en':1}

function main(){
    // Compute the total emission (for proportion computations)
    var tot_emissions = d3.sum(statesData.features, function(d){return d.properties.emissions})

    // Build the table.
    var table = d3.select('#main').append('table')
    table.attr('class','table table-striped')
    table.html('<thead><tr><th>Country</th><th>Emissions (MtCO<sub>2</sub>-eq)</th><th>Ratification Status</th><th>Details</th></tr></thead>')
    var tbody = table.append('tbody')

    // Bind the data to the table.
    var class_status = {signed:'info', ratified:'success', null:'danger', undefined:'warning'}
    var tr = tbody.selectAll('tr')
	.data(statesData.features)
	.enter()
	.append('tr')
	.html(function(d){
	    var code = ratification_status[d.properties.code_status] || ratification_status['default']
	    return ('<tr><th>'+ d.properties.name+'</th><td>'+
		    d.properties.emissions+' ('+(d.properties.emissions/tot_emissions*100.).toFixed(2)+'%)</td>'+
		    '<td style="background-color:'+code.color+'">' + code.name+'</td>'+
		    '<td>'+d.properties['details.'+lang]+'</td></tr>')})

    // By default, sort the countries by emission
    tr.sort(function(a, b) {return b.properties['emissions'] - a.properties['emissions']})

    // Sort a column when clicking on it.
    d3.selectAll('thead th')
	.data(['name','emissions','code_status','details.'+lang])
	.on('click', function(k) {
	    order[k] *= -1
	    if (k=='emissions'){
		tr.sort(function(a, b) {return order[k]*(b.properties['emissions'] - a.properties['emissions'])})
	    }
	    else if (k=='code_status'){
		tr.sort(function(a, b) {
		    var codea = ratification_status[a.properties.code_status] || ratification_status['default']
		    var codeb = ratification_status[b.properties.code_status] || ratification_status['default']
		    //console.log(codea, codeb)
		    return codea.name > codeb.name ? -1*order[k] : codea.name < codeb.name ? 1*order[k] : 0})
	    }
	    else{
	    tr.sort(function(a, b) {
		return a.properties[k] > b.properties[k] ? -1*order[k] : a.properties[k] < b.properties[k] ? 1*order[k] : 0})
	    }
	})
}
