À propos
---------------------

## Qui sommes-nous ?

Nous sommes des jeunes impliqués dans la lutte contre les dérèglements climatiques. Nombre d'entre nous ont pris part activement à la 11e Conférence des Jeunes sur le Climat ([COY11](http://coy11.org)) qui s'est tenue fin novembre 2015, à l'aube de la COP21. 

Non contents d'avoir organisé la plus grande manifestation française de jeunes sur le climat de la décennie, nous nous sentons surtout concernés par l'avenir de la planète et la protection des populations qui y vivent. Nous pensons qu'une mise en œuvre concrète et effective de l'Accord de Paris en constitue un premier pas.

Notre initiative est soutenue notamment par le [REFEDD](http://refedd.org), le Réseau Français des Étudiants pour le Développement Durable.

Contactez-nous : [contact@paris-agreement.fr](mailto:contact@paris-agreement.fr)

