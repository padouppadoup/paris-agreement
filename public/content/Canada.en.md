Canada
------

Following signature, **international treaties are ratified by the
federal Cabinet**.

However, usually, *“the Minister of Foreign Affairs will initiate the
tabling of all instruments, accompanied by a brief Explanatory
Memorandum in the House of Commons following their adoption by signature
or otherwise, and prior to Canada's expression of its consent to be
bound by ratification, acceptance, approval or accession.”* (Sources:
[Parliament](http://www.bdp.parl.gc.ca/content/lop/researchpublications/2008-45-e.htm);
[Global](http://www.treaty-accord.gc.ca/procedures.aspx?lang=eng)[](http://www.treaty-accord.gc.ca/procedures.aspx?lang=eng)[Affairs](http://www.treaty-accord.gc.ca/procedures.aspx?lang=eng)[](http://www.treaty-accord.gc.ca/procedures.aspx?lang=eng)[Canada](http://www.treaty-accord.gc.ca/procedures.aspx?lang=eng))

In fact, **before ratifying it, the Cabinet sends a copy of the
agreement to the Parliament, so that the MPs are informed of the content
of the treaty**. The explanatory document is aimed to describe the
policy impacts of the treaty, its federal-provincial-territorial
implications, considerations about the implementation in Canadian law,
etc.

During a 21-day tabling period, the MPs may demand a debate on the
treaty or a vote on a motion. After this tabling period, the Cabinet
answers to the issues raised by the MPs; the opinion of the Parliament
ist not binding and it is mostly a courtesy practice. The final decision
to ratify is an Order of the Cabinet authorizing the Minister of Foreign
Affairs to deposit the instrument of ratification.


**[On](http://www.parl.gc.ca/HousePublications/Publication.aspx?Language=E&Mode=1&Parl=42&Ses=1&DocId=8245806&File=0#Int-8903087)[6](http://www.parl.gc.ca/HousePublications/Publication.aspx?Language=E&Mode=1&Parl=42&Ses=1&DocId=8245806&File=0#Int-8903087)[May](http://www.parl.gc.ca/HousePublications/Publication.aspx?Language=E&Mode=1&Parl=42&Ses=1&DocId=8245806&File=0#Int-8903087)[2016](http://www.parl.gc.ca/HousePublications/Publication.aspx?Language=E&Mode=1&Parl=42&Ses=1&DocId=8245806&File=0#Int-8903087),
Minister of Environment and Climate Change, Catherine McKenna tabled the
Paris Agreement in the House of Commons** (Sessional Paper No.
8532-421-11). The Opposition MPs may now demand a debate on the
Agreement (but such a debate will use one of their “opposition days”).

[The](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[federal](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[Cabinet](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[is](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[expected](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[to](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[bring](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[the](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[agreement](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[forward](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[for](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[a](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[vote](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[by](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[the](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[fall](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359).
Prime Minister, Justin Trudeau, pledged to ratify the Paris Agreement by
the end of 2016.

## Update on 3 October 2016
Meetings among the federal, provincial and terrritorial ministers for environment are scheduled on 3 October. The MPs will have a debate and vote on the subject on [5 October 2016](http://www.cbc.ca/news/politics/paris-climate-parliament-1.3783047); the vote is not binding. The formal ratification of the Paris Agreement by Canada could occur right after this vote.
However, many observers note that despite this move on the international stage, the federal government of Justin Trudeau sticks to GHG reduction targets which are described as [unambitious and somewhat disappointing](http://www.ctvnews.ca/politics/liberals-back-away-from-setting-tougher-carbon-targets-1.3075857).
