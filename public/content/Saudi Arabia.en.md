Saudi Arabia
------------

**So far, Saudi Arabia has neither signed nor ratified the Paris Agreement.**

Despite the fact that the Like-Minded Group of Developing Countries (LMDC) expressed satisfaction after the negotiation of the Paris Agreement ([by the voice of his spokeperdon Gurdial Singh Nijar](https://www.yahoo.com/news/india-china-saudi-arabia-happy-climate-pact-153738758.html?ref=gs)), Saudi Arabia has not ratified the Paris Agreement yet.

Although many observers argued that Saudi Arabia might be trying to [sabotage the agreement](http://grist.org/climate-energy/is-saudi-arabia-trying-to-sabotage-the-paris-climate-talks/), the kingdom submitted this month its 15-year plan, in which diversification of the economy towards non-oil based activities plays a non-negligible part. This involves:
- selling a few percents of the national oil company Aramco,
- proceeds from this sell will go towards a sovereign wealth fund

Indeed, Deputy Crown Prince Mohammed bin Salman described his country as being addicted to oil and ensured that "we can live without oil by 2020" ([source and analysis](http://www.bbc.com/news/world-middle-east-36131391)).

Finally, [recent  declarations](http://www.climateactionprogramme.org/news/germany_and_saudi_arabia_to_ratify_paris_agreement) indicate that Saudi Arabia might ratify the Paris Agreement before COP22.
