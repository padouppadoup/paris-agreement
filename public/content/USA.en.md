United States of America
------------------------

### How dos the US ratify treaties?

The ratification of treaties by United States of America is ruled by the [Article II of the US Constitution](https://en.wikisource.org/wiki/Constitution_of_the_United_States_of_America#Article_II), Section 2, clause 2

> He shall have Power, by and with the Advice and Consent of the Senate, to make Treaties, provided two thirds of the Senators present concur; and he shall nominate, and by and with the Advice and Consent of the Senate, shall appoint Ambassadors, other public Ministers and Consuls, Judges of the supreme Court, and all other Officers of the United States, whose Appointments are not herein otherwise provided for, and which shall be established by Law: but the Congress may by Law vest the Appointment of such inferior Officers, as they think proper, in the President alone, in the Courts of Law, or in the Heads of Departments.

Indeed, the president may sign a Treaty, but it cannot come into force without the consent of the Senate. However, the US constitution shares a more restrictive vision of a treaty than the one in international law. Thus, in US Law, international treaties can take various forms (according to this note by the Congressional Research Service (2001), [Treaties and Other International Agreements: the role of the United States Senate](http://www.au.af.mil/au/awc/awcgate/congress/treaties_senate_role.pdf)
:

- **Treaties under** U.S. law: this treaties might require a legislative implementation
- **Executive Agreements** under U.S. law, which can be subdivided as follow:
    - *Congressional-executive agreements*: the Congress has authorized the executive power to ratify agreements in the domains of postal conventions, foreign trade, foreign military assistance, foreign economic assistance,atomic energy cooperation, etc. The constitutionality of this type of this type of agreement "seems well established and Congress has authorized or approved them frequently".
    - *Agreements pursuant to treaties*: an agreement that was explicitely or implicitely authorized by a previously ratified treaty. The Congressional Research Service concludes: "The President’s authority to conclude agreements pursuant to treaties seems well established, although controversy occasionally arises over whether particular agreements are within the
purview of an existing treaty"
    - *Presidential or sole executive agreements*: these "politically binding" agreements can be negociated only if they fall under the President's executive authority: foreign policy, armed forces. The President needs to notify the Congress about the ratification of an executive agreement within 20 days. "Courts have indicated that executive agreements based solely on the President’s independent constitutional authority can supersede conflicting provisions of state law, but opinions differ regarding the extent to which they can supersede a prior act of Congress. What judicial authority exists seems to indicate that they cannot."

### Is the Paris Agreement an executive treaty in US law?
Although many conservative organizations have strongly argued that the [Paris Agreement should be considered as a Treaty](http://www.heritage.org/research/reports/2016/03/the-paris-agreement-is-a-treaty-and-should-be-submitted-to-the-senate) and thus should be submitted to the Senate, several arguments point to the fact that it can be considered as an executive agreement, including:

- Harsh negotiation took place in December 2015 on the text of the agreement to make it strictly *non-binding*. This involved [replacing the binding word **should** by the non-binding one **shall**](http://www.climatechangenews.com/2015/12/15/paris-agreement-does-not-need-senate-approval-say-officials/) in the whole text.
- Several US officials declared that since the Paris Agreement relies on "bottom-up targets" (the INDC, "intended nationally determined contributions"), "[they]  have strongly backed the notion of **non-legally binding** targets as the best way to ensure broad participation in the agreement" ([Todd Stern, Special Envoy for Climate Change, Oct. 2015](http://www.foreign.senate.gov/hearings/2015-paris-international-climate-negotiations-examining-the-economic-and-environmental-impacts_102015p)).
- As stated by the Congressional Research Service, deciding whether an international treaty is a an executive agreeement is done most of the time by the executive branch:

> Sometimes it is not clear on the face of a treaty whether it is self-executing or requires implementing legislation. Some treaties expressly call for implementing legislation or deal with subjects clearly requiring congressional action, such as the appropriation of funds or enactment of domestic penal provisions. The question of whether or not a treaty requires implementing legislation or is self-executing is a matter of interpretation largely by the executive branch or, less frequently, by the courts.

### Concerns about the entry into force of the Agreement
Since the Paris Agreement should not require the modification of the US law, several observers have expressed concern about the implementation of the Agreement. Indeed:
- Whether a President opposing the Agreement could withdraw is [still debated](https://www.washingtonpost.com/news/energy-environment/wp/2016/04/11/obamas-fast-move-to-join-the-paris-climate-agreement-could-tie-up-the-next-president/)
- Also, some republican deputies might argue about the [qualification as an executive agreement vs. a treaty](https://www.washingtonpost.com/news/powerpost/wp/2015/11/30/trick-or-treaty-the-legal-question-hanging-over-the-paris-climate-change-conference/)

### Sources

- [Constitution of the United States of America](https://en.wikisource.org/wiki/Constitution_of_the_United_States_of_America#Article_II)
- [Some more information about the Treaty Clause in the US constitution](https://en.wikipedia.org/wiki/Treaty_Clause)
- [Treaties and Other International Agreements: the role of the United States Senate](http://www.au.af.mil/au/awc/awcgate/congress/treaties_senate_role.pdf), page 4 and following
