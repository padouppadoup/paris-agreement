Sources and resources
---------------------

## Resources
- A [map of the INDCs](http://cait.wri.org/indc/), edited by the [World Resource Institute](http://wri.org)
- The [UNFCCC agenda](http://timeglider.com/t/1315169be638a083),
- The [ratification simulation map](http://www.wri.org/blog/2016/04/when-could-paris-agreement-take-effect-interactive-map-sheds-light), edited by the [World Resource Institute](http://wri.org)

## Sources

### Data sets
- The ratification procedures are extracted from Beth Simmons' work [*Mobilizing Human Rights International Law in Domestic Politics*](http://scholar.harvard.edu/bsimmons/files/APP_3.2_Ratification_rules.pdf)
- CO_2 emission values come from [this document](http://unfccc.int/files/ghg_data/application/pdf/table.pdf). These values are regarded as authoritative data for the Paris Agreement ratification.
- The [Paris Agreement signature and ratification webpage on the UN Treaties website](https://treaties.un.org/pages/ViewDetails.aspx?src=TREATY&mtdsg_no=XXVII-7-d&chapter=27&lang=en)


### Information
- The UNFCCC website: http://unfccc.int
- [UN's Treaties Manual](https://treaties.un.org/doc/source/publications/THB/French.pdf), on the UN Treaties website: https://treaties.un.org/
- [Additional information](https://treaties.un.org/doc/Publication/CN/2016/LCremarksF.pdf) about ratification
- [Ban Ki Moon call for ratification](https://treaties.un.org/doc/Publication/CN/2016/CN.63.2016-Frn.pdf)
