Ratification de l'Accord de Paris par la France
-----------------------------------------------

L’accord de Paris est un “accord mixte” en droit européen ; deux procédures de ratification ont donc lieu en parallèle : l’une au niveau de la France, l’autre au niveau de l’UE.

## Mises à jour
- **Mai 2016** : L’Accord a été approuvé par l’Assemblée nationale le 17 mai 2016 et le. La prochaine étape est le vote du Sénat le 8 juin. Le projet de loi autorisant la ratification de l’Accord de Paris pourrait être promulgué le 17 juin.


## Procédure de ratification

En France, les traités internationaux sont ratifiés par le Président de la République ; mais celui-ci a besoin de [l’accord du Parlement lorsque le traité a des implications liées à l’ “organisation internationale”](https://www.legifrance.gouv.fr/Droit-francais/Guide-de-legistique/IV.-Regles-propres-a-certaines-categories-de-textes/4.1.-Textes-internationaux-et-de-l-Union-europeenne/4.1.1-Elaboration-conclusion-et-publication-des-textes-internationaux#ancre3981_0_4).

> [Article 53 de la Constitution](http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/la-constitution/la-constitution-du-4-octobre-1958/texte-integral-de-la-constitution-du-4-octobre-1958-en-vigueur.5074.html#article53) : “Les traités de paix, les traités de commerce, les traités ou accords relatifs à l'organisation internationale, ceux qui engagent les finances de l'Etat, ceux qui modifient les dispositions de nature législative, ceux qui sont relatifs à l'état des personnes, ceux qui comportent cession, échange ou adjonction de territoire, ne peuvent être ratifiés ou approuvés qu'en vertu d'une loi. (...)”

Il faut donc **une loi votée par le Parlement pour ratifier l’Accord de Paris**.

- Le Gouvernement transmet d’abord un projet au Conseil d’État (l’organe chargé de conseiller le Gouvernement dans la préparation des projets de lois). Cette consultation est obligatoire et prend entre 1 et 2 mois (mais le Gouvernement peut demander un examen plus rapide) ; il s’agit notamment de vérifier la façon dont le traité s’articulerait avec le droit français.
- Après que le Conseil d’État ait rendu son avis, le projet de loi est officiellement présenté en Conseil des ministres. Un projet de loi de ratification se présente généralement sous la forme d’un article unique : “Est autorisée la ratification du traité XXX, et dont le texte est annexé à la présente loi. La présente loi sera exécutée comme loi de l’État.”
- [Le projet de loi est examiné successivement par les deux chambres du Parlement](http://www2.assemblee-nationale.fr/decouvrir-l-assemblee/role-et-pouvoirs-de-l-assemblee-nationale/les-fonctions-de-l-assemblee-nationale/les-fonctions-legislatives/la-ratification-des-traites), l’Assemblée nationale et le Sénat, qui doivent l’adopter dans les mêmes termes. Même s’ils peuvent théoriquement faire des amendements, il est peu probable que les députés et sénateurs modifient le projet de loi. De même, les parlementaires peuvent saisir le Conseil constitutionnel pour vérifier la constitutionnalité du traité, mais cela apparaît peu probable (particulièrement dans le cas de l’Accord de Paris).
- Dans chaque chambre, le projet de loi est d’abord examiné par la Commission compétente, puis discuté en séance plénière avant le vote final. Cependant, les traités peuvent bénéficier d’une “procédure d'examen simplifiée” : cette procédure plus rapide, déclenchée à l’initiative de la Commission des affaires étrangères ou du Gouvernement, permet au projet de loi d’être directement mis aux voix sans débat en séance plénière.

Quand les deux chambres du Parlement ont adopté le texte dans les mêmes termes, la loi peut être promulguée : le Président de la République est donc autorisé à ratifier le traité. Toutefois, cette ratification n’intervient pas forcément aussitôt (elle peut par exemple être retardée afin d’avoir une ratification concomitante avec d’autres États ou l’UE).

## Situation actuelle

La ministre française de l’Environnement, Ségolène Royal, a annoncé dans une communication en [Conseil des ministres le 9 mars 2016](http://www.cop21.gouv.fr/communication-en-conseil-des-ministres-de-segolene-royal-presidente-de-la-cop-relative-a-la-mise-en-oeuvre-de-laccord-de-paris/) que le projet de loi autorisant la ratification serait transmis au Conseil d’État “dès après la signature de l’Accord par le président de la République le 22 avril prochain à New York, journée mondiale de la Terre”.

Le projet de loi a été effectivement présenté en Conseil des ministres le 4 mai. L’Assemblée nationale l’a [examiné en procédure accélérée](http://www.assemblee-nationale.fr/14/dossiers/ratification_accord_cop21.asp). 

Après les avis de la Commission des affaires étrangères (avec pour rapporteur Pierre-Yves Le Borgn’) et de la Commission du développement durable (avec pour rapporteur Jean-Paul Chanteguet), les députés ont voté en faveur du projet de loi de ratification de l’Accord de Paris [le 17 mai 2016](http://www.assemblee-nationale.fr/14/cri/2015-2016/20160187.asp#P781559) (à la quasi-unanimité moins l’abstention de deux députés d’extrême-droite).

Le projet de loi a ensuite été transmis au Sénat, où la Commission des affaires étrangères et la Commission du développement durable ont à nouveau donné leur avis. Les sénateurs ont voté à l’unanimité en faveur du projet de loi le [8 juin 2016](http://www.senat.fr/dossier-legislatif/pjl15-614.html). La loi est donc définitivement adoptée par le Parlement.


Ainsi, **la loi autorisant la ratification de l’Accord de Paris sera promulguée “au cours de l’été” 2016**. D’après Le Monde, le président François Hollande promulguerait la loi le 17 juin “La France entend être parmi les premiers États à avoir accompli ses procédures internes de ratification de l’accord de Paris”.

