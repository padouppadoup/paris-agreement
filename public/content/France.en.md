France
--------

## Milestones

- **17 May 2016**: The Agreement has been approved by the French National Assembly on 17 May 2016 and. The next step is the vote of the Senate on 8 June. The law will be signed by President Hollande on 17 June.

As the Paris Agreement is a “mixed agreement” under EU law, France has to ratify the Paris Agreement in parallel to the EU.

## Procedure

In France, international treaties are ratified by the President but [s/he requires the consent of the Parliament if the treaty has implications related to the “international organization”](https://www.legifrance.gouv.fr/Droit-francais/Guide-de-legistique/IV.-Regles-propres-a-certaines-categories-de-textes/4.1.-Textes-internationaux-et-de-l-Union-europeenne/4.1.1-Elaboration-conclusion-et-publication-des-textes-internationaux#ancre3981_0_4).

> [Article 53 of the French Constitution](http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/la-constitution/la-constitution-du-4-octobre-1958/texte-integral-de-la-constitution-du-4-octobre-1958-en-vigueur.5074.html#article53): “Peace Treaties, Trade agreements, treaties or agreements relating to international organization, those committing the finances of the State, those modifying provisions which are the preserve of statute law, those relating to the status of persons, and those involving the ceding, exchanging or acquiring of territory, may be ratified or approved only by an Act of Parliament. [...]”

Consequently, **a law adopted by the Parliament is needed in order to ratify the Paris Agreement**.

- The Government sends a draft bill to the Conseil d’État. The Conseil d’État is an advisory body which provides legal support in the final steps of the preparation of draft bills. This legal review is mandatory and takes about 1 to 2 months (but the Government can demand a faster review); it is aimed to clarify how the treaty would fit in the French legal framework.
- After getting the opinion of the Conseil d’État, the Government formally presents the draft law. Usually, such a draft law contains only the following sentence: “The ratification of the treaty related to XXX, a copy of which is attached, is authorized.”
[The draft law is successively examined by the National Assembly and the Senate](http://www2.assemblee-nationale.fr/decouvrir-l-assemblee/role-et-pouvoirs-de-l-assemblee-nationale/les-fonctions-de-l-assemblee-nationale/les-fonctions-legislatives/la-ratification-des-traites) (which are the two houses of the French Parliament), so that they adopt the law in the same terms. Though MPs can make comments, it is very unlikely that the draft law is modified by any house. Similarly, MPs can demand a review of the compliance of the treaty with the French Constitution by the Constitutional Council, but this appears improbable (especially for the Paris Agreement).
- In each house, the draft law is reviewed by a Commission (a subbody within the house, , e.g. the Commission for foreign affairs), then discussed in plenary meeting before the official vote. However, most international agreements are reviewed under a fast-track procedure: if the Commission for foreign affairs or the Government asks so, no debate occurs in plenary and the draft law is directly put to the vote.

The law is enacted after the vote of the two houses of the Parliament. Then, the President is allowed to ratify the international agreement. Sometimes, this formal ratification is not taken immediately: the President may postpone the ratification so that there is a simultaneous ratification with other states and/or with the EU.

## Current situation

The French minister of Environment, Ms Ségolène Royal, announced on [9 March 2016](http://www.cop21.gouv.fr/en/implementation-of-the-paris-agreement/) that the draft bill on the ratification of the Paris Agreement would be sent to the Conseil d’État “as soon as French President Hollande signs the Agreement on 22 April in New York on Earth Day”.
The draft law was indeed formally presented by the Government on 4 May. 
[The National Assembly examined it](http://www.assemblee-nationale.fr/14/dossiers/ratification_accord_cop21.asp) under the fast-track procedure; after positive opinions of the Commission for foreign affairs and the Commission for sustainable development, the French MPs voted in favour of the draft law on the ratification of the Paris Agreement on 17 May 2016 (almost unanimously, except 2 far-right MPs who abstained).

Then [the draft law was sent to the Senate](http://www.senat.fr/espace_presse/actualites/201405/les_senateurs_preparent_la_conference_paris_climat_2015_cop21.html). Again, the Commission for foreign affairs and the Commission for sustainable development reviewed the bill, then the Senators voted unanimously in favour of the draft law on 8 June 2016

Thus, **the law authorizing the ratification of the Paris Agreement would be enacted “sometime this summer” 2016**.  According to French newspapers, President Hollande is expected to sign the law on 17 June: “France intends to be one of the first States to complete its domestic Paris Agreement ratification procedures.”
