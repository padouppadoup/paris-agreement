Qu'est-ce que l'Accord de Paris ?
----------------------------------

Les parties à la Convention Cadre des Nations Unies sur les Changements Climatiques (la CCNUCC) sont invitées à se retrouver en moyenne une fois par ans lors des COP (les Conférence des Parties). En 2015, la 21è COP a eu lieu à Paris du 30 novembre au 12 décembre sur le site du Bourget. Lors de cette COP, l’Accord de Paris a été adopté par consensus par l’ensemble des 196 parties.

L’Accord de Paris et les résultats de la conférence climatique de l’ONU (COP21) couvrent tous les domaines primordiaux  :

- *Atténuation* - réduire les émissions suffisamment vite pour atteindre l’objectif de température
- *Un système de transparence et de bilan mondial* - comptabilité de l’action climatique
- *Adaptation* - renforcer la capacité des pays à faire face aux impacts climatiques
- *Pertes et dommages* - renforcer la capacité à se remettre des impacts climatiques
- *Soutien* - dont les financements pour que les nations construisent des avenirs propres et résilients

Le document peut être [lu en français en ligne](http://unfccc.int/resource/docs/2015/cop21/fre/l09f.pdf) sur le site de la CCNUCC.  
L'Accord de Paris proprement dit est un document de 17 pages, précédé d'une « décision de la COP » de 22 pages, qui précise son contenu, prolonge certains thèmes et en aborde d'autres. Ces deux documents ont un statut très différent, et seul l'accord est juridiquement contraignant : la décision n'a quant à elle qu'une valeur indicative.
