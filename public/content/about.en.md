About
---------------------

## Who are we?

We are a bunch of young people and we believe that climate change is a major issue of our generation. Many of us got involved into the organization of the 11th Conference of Youth ([COY11](http://coy11.org)) which took place at the end of November 2015, just before COP21.

The COY11 was one of the biggest French youth & climate-oriented meeting in the last decade, but first and foremost we feel deeply concerned about the future of our planet and its inhabitants. We believe that an effective enforcement of the Paris Agreement is the first step to address this issue.

Our project is supported by the [REFEDD](http://refedd.org), the French Student Network on Sustainable Development.

Contact us: [contact@paris-agreement.fr](mailto:contact@paris-agreement.fr)