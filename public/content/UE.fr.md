Procédure de ratification de l'Accord de Paris par l'Union Européenne
---------------------------------------------------------------------

## Procédure
[L’adoption d’un accord international par l’Union européenne requiert deux décisions](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=URISERV%3Al14532) :
- la décision de signer l’accord (pouvant emporter application provisoire de l’accord) ;
- la décision de conclure l’accord (qui constitue formellement la ratification de cet accord).

L’environnement est une “compétence partagée” entre l’UE et les États-membres. Par conséquent, tout accord international relatif à l’environnement négocié par l’UE doit être ratifié par l’UE mais aussi chaque État-membre selon ses propres règles constitutionnelles : on parle alors d’ “accord mixte”. **La ratification de l’Accord de Paris par l’UE aura lieu en parallèle de 28 procédures de ratification nationales**.

Les règles applicables à la signature et la ratification sont similaires ; elles sont décrites à l’[article 218 du Traité sur le Fonctionnement de l’Union Européenne](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=CELEX:12008E218). 
En ce qui concerne la ratification, **la Commission prépare une proposition de décision du Conseil de l’Union europénne (ou Conseil)** concluant l’Accord de Paris.

- D’une part, la proposition émise par la Commission sera examinée au sein du Conseil par un comité technique, puis par le Comité des Représentants Permanents (COREPER).
- D’autre part, la proposition doit obtenir l’approbation du Parlement européen (car l’environnement est un champ couvert par la “procédure législative ordinaire” et que l’Accord de Paris crée un un cadre institutionnel spécifique et peut avoir des implications budgétaires notables). Cette étape peut être longue, mais le Conseil peut fixer un délai au Parlement pour se prononcer.
- Enfin, **le Conseil adoptera sa décision de conclure l’Accord à la majorité qualifiée**. Les sessions du Conseil sont diffusées en ligne en direct.

## Situation en mai 2016

**La décision de signer l’Accord de Paris au nom de l’Union européenne a été publiée au Journal Officiel de l’UE le [19 avril 2016](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32016D0590&from=FR)**.

**Pour l’heure, la Commission européenne n’a pas encore publié de document spécifiquement sur la conclusion de l’Accord de Paris**. La préparation d’un tel document aura probablement lieu après le 22 avril 2016.
Les institutions européennes ont souligné leur intention de ratifier l’Accord de Paris le plus tôt possible :

- communiqué de la Commission européenne, [2 mars 2016](http://europa.eu/rapid/press-release_IP-16-502_fr.htm): “La ratification et l’entrée en vigueur rapides de l'accord de Paris fourniront aux États la sécurité juridique que cet accord sera rapidement opérationnel. Il est donc souhaitable que celles-ci interviennent le plus tôt possible.”
- conclusions du Conseil Environnement, [4 mars 2016](http://www.consilium.europa.eu/fr/meetings/env/2016/03/04/): “Les ministres ont également insisté sur la pertinence d'une ratification rapide de l'accord.”
- European Council conclusions, [17-18 mars 2016](http://data.consilium.europa.eu/doc/document/ST-12-2016-REV-1/fr/pdf): “Le Conseil européen [...] souligne que l'Union européenne et ses États membres doivent être en mesure de ratifier l'accord dans les meilleurs délais et à temps pour y être parties dès son entrée en vigueur.”

## Mise à jour : 3 octobre 2016

Alors que les principaux pays émetteurs ont accéléré le rythme des ratifications, avec la possibilité d’une entrée en vigueur de l’Accord avant fin 2016, des [inquiétudes grandissantes](http://www.slate.fr/story/121603/climat-accord-de-paris-europe-ou-es-tu) sont apparues quant à la capacité de l’UE à ratifier l’Accord de Paris dans les temps, c’est-à-dire avant le 7 octobre 2016. En effet, si les seuils de 55 Etats représentant 55% des émissions mondiales sont atteints à cette date sans l’UE, l’UE pourrait ne pas pouvoir participer aux premières décisions liées à l’application de l’Accord de Paris. Une telle situation serait un signal négatif sur le leadership climatique européen. 

Le [8 septembre](http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//TEXT+IM-PRESS+20160907IPR41562+0+DOC+XML+V0//FR), la commission environnement du Parlement européen a recommandé l’approbation de la ratification de l’Accord de Paris. 

**Lors de la réunion du Conseil Environnement le [30 septembre 2016](http://www.consilium.europa.eu/en/meetings/env/2016/09/30/), les vingt-huit ministres de l’environnement ont voté à l’unanimité en faveur de la ratification de l’Accord de Paris par l’UE.**

Un tel calendrier passe pour être une procédure rapide au regard des délais habituels dans l’UE. 
La présidence slovaque du Conseil a publié [un communiqué](http://www.consilium.europa.eu/en/press/press-releases/2016/09/30-council-speeds-eu-ratification-paris-agreement/), précisant que *“les Etats-membres ratifieront soit avec l’UE s’ils ont achevé leurs procédures nationales, soit ultérieurement dès que possible”*. D’après le communiqué, 4 Etats-membres avaient achevé leur procédure nationale début octobre 2016 : la France, la Hongrie, l’Autriche et la Slovaquie. Une déclaration similaire a été publiée par la Commission européenne.

Après la décision du Conseil Environnement, le Parlement européen donnera un vote conforme lors de la plénière du [4 octobre 2016](http://www.europarl.europa.eu/news/fr/news-room/20160930IPR44808/accord-de-paris-le-parlement-votera-sur-la-ratification-europ%C3%A9enne). **L’envoi formel de l’instrument de ratification de l’UE auprès des Nations Unies pourrait ainsi intervenir avant le 7 octobre**.


## Débats et enjeux

Il apparaît que **l’UE pourrait soumettre sa ratification aux Nations Unies et devenir une partie de l’Accord de Paris même sans que tous ses États membres aient ratifié l’Accord**. Cette hypothèse est déjà prévue par l’Accord de Paris (articles 20 et 25).

- Certes, le site du Conseil rappelle que la décision finale de conclure un accord “ne peut se faire qu'après (...) que tous les États membres de l'UE ont procédé à la ratification de l'accord.”
- Cependant, l’UE a déjà adopté une décision de conclusion de l’amendement de Doha au protocole de Kyoto le 13 juillet 2015, alors que certains États membres ne l’avaient pas encore ratifié. De même, la Convention de l’ONU sur le droit de la mer a été ratifiée par l’UE - avec notification formelle à l’ONU en 1998, tandis que la ratification du Danemark n’est pas intervenu avant 2004. 

Une ratification d’accord international par l’UE prend généralement du temps ; quel que soit son sujet, cette procédure peut aisément durer plus de deux ans. **Certains observateurs se sont inquiétés de la possibilité que [l’Accord de Paris puisse entrer en vigueur sans l’UE](http://www.euractiv.fr/section/climat-environnement/news/laccord-de-paris-sur-le-climat-pourrait-entrer-en-vigueur-sans-lue/)**. 

Au-delà de la coordination des procédures de ratification entre le niveau européen et le niveau national, des débats ont eu lieu ([y compris entre les ministres](http://www.consilium.europa.eu/fr/meetings/env/2016/03/04/)) pour savoir **si l’UE peut ratifier ou non l’Accord de Paris avant de trancher la question de la répartition des efforts** dans la réduction des émissions au sein de l’UE.

- Option 1 (portée notamment par l’[Irlande](http://video.consilium.europa.eu/fr/webcast/c30051e1-e264-4300-bf1b-e52532729bc5)): Pas de précipitation : il faut d’abord se mettre d’accord sur la répartition des efforts et l’adaptation des politiques communes avant de conclure l’Accord de Paris. 
- Option 2 (portée notamment par la [France](http://video.consilium.europa.eu/fr/webcast/c30051e1-e264-4300-bf1b-e52532729bc5)):  L’UE devrait ratifier l’Accord de Paris rapidement, le débat sur la répartition des efforts intervenant après la ratification.

Il peut être déduit des conclusions du Conseil européen des [17-18 mars 2016](http://data.consilium.europa.eu/doc/document/ST-12-2016-REV-1/fr/pdf) (cf. supra) que la position française est actuellement privilégiée, rendant plausible une ratification plutôt “rapide”.

Quel que soit le choix retenu, **l’UE aura à réexaminer ses politiques afin de les mettre en cohérence avec l’Accord de Paris**. Ces révisions amèneront des débats entre gouvernements nationaux et avec la Commission ; il y a actuellement des désaccords sur la nécessité de relever l’ambition de la politique climatique de l’UE, qui a été conçue en vue de la cible des +2°C et dont l’objectif est pour le moment une réduction de 40% des émissions d’ici 2030. 
