Ratification
---------------------

## Signature of the Treaty
After COP21 adoption, the Paris Agreement will be deposited at the United Nations in New York City and opened to signature on 22 April, 2016, during Earth Day. Parties will have one year to sign the agreement. Neverthelesse, signature single-handedly is not enough for the Paris Agreement to be effective.

## Ratification procedure
Countries will have one year to sign the agreement. The signature reflects the intention of a state to be part of the Agreement and is a prerequisite for ratification.

Ratification procedures vary accross countries, according to their domestic rules. A ratification can be an individual chief executive or cabinet decision; a notification to the parliament about signed treaties; a consent of the Parliament (which may be either a single majority or a supermajority, in one or two legislative bodies); or even a national referendum.

Each country should thereafter deposit its “ratification, acceptance, approbation, accession”  tool with the Secretary-General of the United Nations. In international law, “acceptance” means that the state consent is expressed only because of its signature. Ratification and approbation are equivalent procedures which require two steps: first, a political step (the non-binding signature of the Agreement shows a political commitment); then a legal step (making the Agreement a legal binding instrument in this country). 

## Coming into force
The Paris Agreement needs to be ratified, accepted and approved or accessed by at least 55 parties to the Convention accounting for at least 55% of the global greenhouse gas emissions. The Agreement will come into force on the thirtieth day after the fulfilment of this condition.

