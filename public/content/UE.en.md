Procedure for the adoption of international agreements by the European Union
--------------------------------------------------------------------------

## Procedure
[The adoption of an international agreement by the EU requires two decisions](http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=URISERV%3Al14532):

- a decision on the signing of the agreement (which can be accompanied by the provisional application of the agreement);
- a decision on the conclusion of the agreement (which is deemed to constitute the formal ratification of the agreement).

Environment is a field in which the EU has shared competence with the Member States. Consequently any international agreement related to environment which is negotiated by the EU has to be ratified by each Member State in accordance with their respective constitutional rules: such an agreement is called a “*mixed agreement*”. **The ratification of the Paris Agreement by the EU will occur in parallel of 28 national ratification procedures**.

The procedures for the signing and the ratification are similar and are described at the [article 218 of the Treaty of the Functioning of the European Union](http://eur-lex.europa.eu/legal-content/EN/ALL/?uri=CELEX:12008E218). 
Regarding the ratification, **the Commission will submit a proposal for a Council decision to conclude the Paris Agreement.**

- On the one hand, the Commission proposal will be reviewed within the Council by a technical committee, then by the Committee of Permanent Representatives (COREPER).
- On the other hand, the proposal needs to get approval from the European Parliament (because environment is a field covered by the so-called “ordinary legislative procedure” and the Paris Agreement creates a new institutional framework and has notable budget implications for the EU). This step can take a while, but there is a fast-track allowing the Council to set a deadline for the vote of the Parliament.

Eventually, **the Council will adopt the decision to conclude the Agreement**. Council meetings are live broadcast.

## Situation in May 2016

**The decision on the signing of the Paris Agreement on the behalf of the European Union has been published** in the [Official Journal of the EU on 19 April 2016](http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32016D0590).

**No formal proposal has been issued yet by the European Commission regarding the conclusion of the Paris Agreement**. Such a document will be probably prepared after the signing of the Agreement.

- The European Union institutions have highlighted their will to ratify the Paris Agreement as soon as possible, so that the EU can maintain its “first mover advantage”:
European Commission communiqué, [2 March 2016](http://europa.eu/rapid/press-release_IP-16-502_en.htm): “The early ratification and entry into force of the Paris Agreement will give the legal certainty that the Agreement begins operating quickly. Therefore, the Paris Agreement should be signed and ratified as soon as possible.”
- Environment Council conclusions, [4 March 2016](http://www.consilium.europa.eu/fr/meetings/env/2016/03/04/): “Ministers also emphasised the relevance of swift ratification of the [Paris] agreement.”
- European Council conclusions, [17-18 March 2016](http://data.consilium.europa.eu/doc/document/ST-12-2016-REV-1/en/pdf): “The European Council [...] underlines the need for the European Union and its Member States to be able to ratify the Paris Agreement as soon as possible and on time so as to be Parties as of its entry into force.”

## Update: 3 October 2016

While other major emitters speeded up the pace of their ratification processes (with a plausible entry into force of the Agreement by the end of 2016), [increasing concerns](https://www.euractiv.com/section/climate-environment/news/us-and-china-ratify-paris-climate-pact-leaving-eu-behind/) appeared on the ability of the EU to ratify the Paris agreement on due time, i.e. before 7 October 2016. If the thresholds of 55% parties accounting for 55% of global GHG emissions were met without the EU by that date, the EU could be left aside of the early decisions of the Paris agreement. Such a situation would be a negative signal about the European climate action dynamism.

On [8 September](http://www.europarl.europa.eu/sides/getDoc.do?type=IM-PRESS&reference=20160907IPR41562&language=EN&format=XML), the environment committee of the European Parliament has reviewed the Paris agreement and gave green light for an approval of its ratification.

**The European ministers for environment have voted unanimously in favour of the ratification of the Paris Agreement by the European Union during the Environment Council meeting on [30 September 2016](http://www.consilium.europa.eu/en/meetings/env/2016/09/30/)**.

This has been a fast-track procedure of ratification according to the EU standards.
The Slovak presidency of the Council of the European has released [a statement](http://www.consilium.europa.eu/en/press/press-releases/2016/09/30-council-speeds-eu-ratification-paris-agreement/), saying that “*Member states will ratify either together with the EU if they have completed their national procedures, or as soon as possible thereafter*”. In early October, 4 Member states had completed their ratification according to this statement: France, Hungary, Austria and Slovakia. A similar communiqué has been published by the [European Commission](http://europa.eu/rapid/press-release_STATEMENT-16-3265_fr.htm).

After the decision of the Environment Council, the European Parliament will give its consent by a vote during its plenary on [4 October 2016](http://www.europarl.europa.eu/news/en/news-room/20160930IPR44808/paris-climate-agreement-ep-to-vote-on-eu-ratification-after-council-green-light). Thus, a formal decision to ratify the agreement and the submission of the EU instrument of ratification may be completed before 7 October.

## Debates and issues

It appears that **the European Union could submit its ratification to the United Nations and become a party of the Paris Agreement even if some Member States have not ratified the Agreement yet**. This possibility is already acknowledged by the Paris Agreement (articles 20 and 25).
- The website of the Council states that the final decision on the conclusion of an agreement “can only be done once (...) it has been ratified by all EU member states.”
- However, the EU adopted the decision to conclude the Doha Amendment to the UNFCCC Kyoto Protocol on 13 July 2015, while ratifications were not completed yet in all Member States. Similarly, the UN Convention on the Law of the Sea has been ratified by the EU - with a formal confirmation to the UN in 1998, while Denmark had not ratified it before 2004.

Ratifications of international agreements by the EU use to be lengthy; no regards to its subject, these processes could easily need more than two years. **Some observers have raised concern that the Paris Agreement could come into force [without the EU being part of it](https://www.euractiv.com/section/climate-environment/news/could-the-cop-21-agreement-come-into-force-without-the-eu/)**. 

Beyond the issue of the coordination between the procedures of ratification at the European level and at the national level, there have been debates ([including at the ministers’ level](http://www.consilium.europa.eu/en/meetings/env/2016/03/04/)) about **whether the EU can ratify the Paris Agreement before the discussions about the share of efforts to meet the EU’s emissions reduction commitments.** 

- Option 1 (pushed by [Ireland](http://video.consilium.europa.eu/en/webcast/c30051e1-e264-4300-bf1b-e52532729bc5)): The EU should not rush; the Member States have to agree on the efforts sharing and adapt the EU legislation before concluding the Paris Agreement.
- Option 2 (pushed by [France](http://video.consilium.europa.eu/en/webcast/c30051e1-e264-4300-bf1b-e52532729bc5)): The EU should ratify the Paris Agreement before discussing the efforts sharing.
It can be inferred from the conclusions of the European Council meeting on 17-18 March 2016 (see above)  that France’s position is favored, making a rather “quick” ratification plausible.

It can be inferred from the [conclusions of the European Council meeting on 17-18 March 2016](http://data.consilium.europa.eu/doc/document/ST-12-2016-REV-1/en/pdf) (see above)  that France’s position is favored, making a rather “quick” ratification plausible.

Should the ratification be done before discussing its policy implications, **the EU will have to revise a large part of its legislation in order to be consistent with the Paris Agreement**. These revisions will lead to debates among the national governments and the Commission; there are disagreements about whether the EU should raise the ambition of its own climate action framework, which was designed for a +2°C long-term goal and sets for the moment a target of 40% reduction in emissions by 2030.
