Cameroon
------

## Party

Cameroon is a member of several groups including:

* The Commonwealth ;
* The International Organisation of the Francophonie (OIF);
* The African Union;
* The World Trade Organization (WTO);
* Organization Of Islamic Conference (OIC) ;

## Ratification procedure

At the Cameroon, the Ministry of Foreign Affairs (MINREX) that handles all international treaties and agreements in all Benin concerning treaties and international agreements. Depending on the specifics and sectors in which these treaties and agreements allude MINREX can work with other departments. This is the case for example with the ratification of the Paris Agreement . In this case, the MINREX and the Ministry of Environment, Nature Conservation and Sustainable Development (MINEPDED) worked closely together. The Paris Agreement was signed by the President of the Republic of Cameroon . After the signature, a bill will be prepared by the two ministries and be presented to the President of the Republic before being sent to the National Assembly for a vote by MPs and senators in plenary in their different rooms Before submission to the vote, the various commissions of the two chambers of the Cameroonian parliament involved in this type of case study it first. And it is only when the file was considered acceptable it is made ​​available to MPs and senators in plenary. Once ratified , the new law will be promulgated by the President of the Republic of Cameroon . The Ministry of Forestry and Wildlife and other departments will be involved in drafting this law. The dossier to be submitted to parliamentary vote consists among others of the bill and a document outlining the need " for Cameroon to ratify this law.
This law ratified takes effect only when Cameroon sends a file with this law in the United Nations.

## Current situation
Cameroon has already signed the Paris Agreement, April 22, 2016 New York. This first stage marks the beginning of a process that will continue with the submission of the Paris agreement for ratification to parliament in its session in June 2016 according to a statement from the Secretary General at the Presidency of the Republic (SGPR) read on the occasion of the signing of this agreement by Cameroon.
The major obstacle that will stop the ratification of this agreement, Cameroon is slow at the Cameroonian administration in the treatment of cases. However, when the will is real at the Presidency of the Republic, things go much faster.

## Sources
Ministry of Environment, Nature Conservation and Sustainable Development (MINEPDED) and Secretariat General of the Presidency of the Republic (SGPR).
