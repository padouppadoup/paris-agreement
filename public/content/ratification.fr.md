Ratification
---------------------

## Signature de l’Accord de Paris
Après son adoption par la COP21 (21è Conférence des Parties), l’Accord de Paris sera déposé aux Nations Unies à New York et ouvert à la signature le 22 avril 2016, Journée de la Terre-Mère. Les Parties ont alors un an pour signer l’accord. Néanmoins la seule signature des Partie ne permet pas l’entrée en vigueur de l’Accord de Paris;

## Procédure de ratification de l’accord de Paris
Les pays auront un an pour signer l’accord. La signature reflète l’intention d’un pays à être tenu par un accord et constitue une étape préalable à la ratification.

Les procédures de ratification diffère suivant les Etats. Cela peut être une décision individuelle du ou de la chef.fe de l'exécutif ou du cabinet, nue simple information du corps législatif ou encore le consentement à la majorité relative ou absolue de l’une ou des deux chambres législative, voire un référendum.

Chaque pays devra par la suite déposer formellement son instrument de « ratification, d’acceptation, d’approbation ou d’accession » auprès du Secrétaire général des Nations Unies.
Dans le droit international, on parle d' « acceptation » pour désigner le cas où le consentement de l’Etat est exprimé par sa seule signature. La ratification et l’approbation sont, elles, des procédures équivalentes qui se déroulent en deux temps : d’abord une signature du texte, non engageante, qui exprime un engagement politique (comme ce sera le cas le 22 avril), puis une phase juridique.

## Entrée en vigueur
L’Accord de Paris entrera en vigueur 30 jours après la ratification par au moins 55 pays représentant au total 55 % des émissions mondiales de gaz à effet de serre (GES).