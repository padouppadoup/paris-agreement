Sources et ressources
---------------------

## Sources
Le travail fourni pour la création de cette cartographie s’est fondée sur plusieurs ressources : 

### Données
- Les procédures de ratification trouvées dans l’[annexe du travail de Beth Simmons dans son ouvrage Mobilizing Human Rights International Law in Domestic Politics](http://scholar.harvard.edu/bsimmons/files/APP_3.2_Ratification_rules.pdf) 
- Les valeurs d'émissions par pays sont celles [considérées comme pertinentes par la CCNUCC](http://unfccc.int/files/ghg_data/application/pdf/table.pdf). Elles peuvent différer des dernières valeurs d'émission publiées par certains pays.
- Les données concernant la signature et le dépôt des instruments de ratification se trouvent sur la [page de l'Accord de Paris sur le site des traités des Nations Unies](https://treaties.un.org/pages/ViewDetails.aspx?src=TREATY&mtdsg_no=XXVII-7-d&chapter=27&lang=en).

### Informations
- Le site de la CCNUCC : http://unfccc.int
- Le site de suivi des traités des Nations Unies : https://treaties.un.org/ et notamment [le manuel des traités](https://treaties.un.org/doc/source/publications/THB/French.pdf)  (annexe 4 pour la ratification)
- Explication du conseiller de ratification : https://treaties.un.org/doc/Publication/CN/2016/LCremarksF.pdf  
- L’[appel de Ban Ki Moon à la cérémonie de ratification](https://treaties.un.org/doc/Publication/CN/2016/CN.63.2016-Frn.pdf)

## Ressources
- La [carte des INDC](http://cait.wri.org/indc/) a priori à jour
- La [frise chronologique](http://timeglider.com/t/1315169be638a083) de la CCNUCC
- Le WRI et sa [carte de simulation de ratification de l’Accord de Paris](http://www.wri.org/blog/2016/04/when-could-paris-agreement-take-effect-interactive-map-sheds-light)  
