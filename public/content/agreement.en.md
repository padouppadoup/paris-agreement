What is the Paris Agreement ?
-----------------------------

Parties to the United Nations Framework Convention on Climate Change (UNFCCC) are invited to meet up at least once in a year. In 2015, COP21 took place in Paris from 30 November 2015 to 12 December 2015. At the end of this COP, the Paris Agreement has been adopted by consensus by all the 196 Parties.

The Paris Agreement and its results cover all the following vital areas:

- *Mitigation* – reduce emissions fast enough in order to comply with the temperature goal
- *Transparency mechanism* – make climate action accountable
- *Adaptation* – enhance state capacity to address climate change impacts
- *Loss and damages* – enhance capacity to recover from climate change impacts
- *Support* – provide funds so that communities can build appropriate and resilient futures

The Paris Agreement can be [read in English](http://unfccc.int/files/meetings/paris_nov_2015/application/pdf/paris_agreement_english_.pdf) on the UNFCCC website. The Paris Agreement is a 17-page document. It goes along with a 22-page “Decision adopting Paris Agreement” which clarifies the content of the Agreement, goes into some topics in depth, and raises issues on others. Those two documents have different status: the Agreement is the sole legally enforceable instrument, while the Decision only has an indicative status.
