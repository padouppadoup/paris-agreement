Bénin
------

## Party

Benin is part of three (3) groups such as :

* The Group of Least Developed Countries (LDCs)
* The Africa Group
* The Group G 77 and China

## Ratification procedure

In Benin everything concerning treaties and international agreements are under the responsibility of the Ministry of Foreign Affairs and Cooperation, although each Ministry has its own department that works with the Ministry of Foreign Affairs and Cooperation, on matters within its field.
 
Regarding the ratification of Paris Agreement, the Minister of Foreign Affairs and Cooperation in partnership with the Ministry of the living environment and sustainable development are in charge of the procedure. A bill must be adopted by the National Parliament for the ratification of the Paris agreement. The dossier to introduce to the parliament is mounted by the Minister of Foreign Affairs and Cooperation in partnership with the Ministry of the living environment and sustainable development.
 
The dossier is then examined by the other governments departments and in Council of Ministers. Subsequently, the government (Head of State) through a decree request for authorization of ratification of the Paris Agreement introduce the dossier at the Parliament for voting. The dossier is composed by the text of the agreement, the decree and the justification of the need for ratification of the agreement by Benin, which is the most important part because the government has through its dossier to convince honorable deputies.
 
After exam of the dossier, the Law Commission of the national assembly must produce a report to the plenary, given its favorable opinion for the ratification. The bill project will therefore be object to a vote in deputies’ plenary session.
 
The Constitutional Courses is consulted after the vote of the bill project by the national assembly, to verify conformity of the agreement with Benin's constitution. This course has two weeks at most, to provide its report and declare its verdict. If the law / Agreement is declared to be constitutional, the government (Head of State), promulgates it by a decree and ensures its publication in the Official Journal.

The last step of the process and that really mark the ratification of the Paris agreement, is when the government (Head of State), send to the General Secretary of the United Nations (UN), the Paris Agreement ratification tools of Benin through an administrative letter. By this time one can said that Benin has really ratified Paris agreement.

## Current situation

Benin has already signed the Paris Agreement, on April 22, 2016 in New York. This first step marks the real engagement of Benin, to ratify the agreement. A report will be made to the Government in council of Ministers to launch the process of ratification. For now, the dossier is not yet mounted. Given this real desire and commitment of the Beninese government, the main constraint to the ratification process is the slowness of administrative procedures and the separation of powers between the executive and the legislature. However, there are remedial measures allowing the government to boost the process at the same level of parliament that remains after the government the main stakeholder in the ratification of the Paris Agreement in Benin.

## Sources

General Direction of Climates Changes (Benin Ministry of living environment and sustainable development) 
