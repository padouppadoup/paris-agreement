How can I help?
-------------------

Our initiative is a totally collaborative project. We value teamwork and mutual learning. We are open to any new member, irrespective of any distinction.

### Help us to keep the website up-to-date
Help us to review the last updates regarding signatures and ratifications in various countries.

### Write a post on your country
The ratification procedure can vary greatly from a country to another . In some of them, it is more a matter of politics than law. A better understanding of forces applying towards and against ratification is key to understand and map the situation. We have some template questions and basic resources to begin with. Therefore, you are welcome to write a post about your country ratification procedure.

### Help us to develop the website
This website relies on free/open source code and technologies. You can contribute to the code of the platform on Gitlab, using [this repository](https://gitlab.com/padouppadoup/paris-agreement).

### You want to contribute but don't know how?
Join our next (online) meeting! We hold bi-monthly meetings, any contributor is welcome.

### How can I contact you?
Just email us at [contact@paris-agreement.fr](mailto:contact@paris-agreement.fr).