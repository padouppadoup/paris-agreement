Comment participer ?
-------------------

Le projet *Paris-Agreement.fr* est collaboratif. Nous travaillons en équipe, sommes ouverts à toutes et tous sans discrimination aucune, et attachons beaucoup d'importance à la bienveillance et à la montée en compétences de chacun.e.

### Mettre à jour le site internet régulièrement
Réaliser une veille juridique et médiatique sur les dernières avancées concernant la signature et la ratification d'un groupe de pays.

### Écrire une fiche sur la ratification dans votre pays
Les procédures de ratifications peuvent varier énormément d'un pays à l'autre, et souvent des éléments politiques interfèrent avec les éléments juridiques. Une analyse approfondie et un suivi de l'actualité sont souvent nécessaire pour bien cartographier les forces en présence. Nous avons une fiche type et des documents ressource pour commencer sur chaque pays.

### Contribuer au développement web
Le site internet a été créé à partir de technologies libres/open source. Chacun peut contribuer au code source par le biais du [dépôt `paris-agreement`](https://gitlab.com/padouppadoup/paris-agreement) sur la plateforme Gitlab.

### Je ne sais pas quoi faire/ce que je peux apporter
Le plus simple est sans doute de participer à notre prochaine réunion pour découvrir l'équipe. Nous avons en moyenne deux réunions (en ligne) par mois, chacun.e est le bienvenu !

### Comment vous contacter ?
Nous sommes joignables à l'adresse [contact@paris-agreement.fr](mailto:contact@paris-agreement.fr).