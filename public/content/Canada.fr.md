Canada
------

Après leur signature, **les traités internationaux sont ratifiés par le
Cabinet fédéral**.

Cependant, la tradition est que *“le ministre des Affaires étrangères
dépose tous les traités, avec un bref mémoire explicatif, devant la
Chambre des communes, après leur signature ou autrement, mais avant que
le Canada n'exprime son consentement à être lié par ratification,
acceptation, approbation ou adhésion.”* (Sources:
[Parliament](http://www.bdp.parl.gc.ca/content/lop/researchpublications/2008-45-f.htm);
[Affaires](http://www.treaty-accord.gc.ca/procedures.aspx?lang=fra)[](http://www.treaty-accord.gc.ca/procedures.aspx?lang=fra)[mondiales](http://www.treaty-accord.gc.ca/procedures.aspx?lang=fra)[](http://www.treaty-accord.gc.ca/procedures.aspx?lang=fra)[Canada](http://www.treaty-accord.gc.ca/procedures.aspx?lang=fra))

En pratique, **avant la ratification, le Cabinet transmet le traité au
Parlement, de sorte que les députés sont informés du traité**. La note
explicative présente l’incidence sur les politiques, la répartition
entre le gouvernement fédéral et les provinces et territoires, la mise
en oeuvre en droit canadien, etc.

Pendant une période de 21 jours après le dépôt, les députés peuvent
demander qu’un débat se tienne sur le traité. À l’issue de cette
période, le Cabinet répond aux enjeux éventuellement soulevés par les
députés ; l’avis du Parlement n’est toutefois qu’indicatif ni même
obligatoire. La décision finale de ratification prend la forme d’un
décret du Cabinet autorisant le ministre des Affaires étrangères à
déposer l’instrument de ratification.


**[Le](http://www.parl.gc.ca/HousePublications/Publication.aspx?Language=F&Mode=1&Parl=42&Ses=1&DocId=8245806&File=0#Int-8903087)[6](http://www.parl.gc.ca/HousePublications/Publication.aspx?Language=F&Mode=1&Parl=42&Ses=1&DocId=8245806&File=0#Int-8903087)[mai](http://www.parl.gc.ca/HousePublications/Publication.aspx?Language=F&Mode=1&Parl=42&Ses=1&DocId=8245806&File=0#Int-8903087)[2016](http://www.parl.gc.ca/HousePublications/Publication.aspx?Language=F&Mode=1&Parl=42&Ses=1&DocId=8245806&File=0#Int-8903087),
la ministre fédérale de l’Environnement et des Changements climatiques
Catherine McKenna a déposé l’Accord de Paris à la Chambre des communes**
(document parlementaire n° 8532-421-11). Les députés des partis
d’opposition peuvent donc demander qu’un débat soit organisé sur
l’Accord (mais ils devront l’organiser sur les jours réservés à
l’Opposition).

[Le](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[gouvernement](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[fédéral](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[envisagerait](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[d](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[’](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[organiser](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[un](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[vote](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[à](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[la](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[Chambre](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[des](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[communes](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[à](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[l](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[’](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[automne](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)[.](http://www.cbc.ca/news/politics/environment-paris-agreement-climate-1.3571359)
L’objectif affirmé par le Premier ministre Justin Trudeau est une ratification d’ici la fin de l’année 2016.

## Mise à jour du 3 octobre 2016 
La ministre fédérale de l’Environnement consulte ses homologues provinciaux et territoriaux le 3 octobre. Un débat suivi d’un vote (non contraignant) aura lieu à la Chambre des communes le [5 octobre](http://ici.radio-canada.ca/nouvelles/environnement/2016/09/30/001-accord-paris-cop21-rechauffement-climatique-ue-canada-cop22.shtml). La ratification formelle de l’Accord de Paris par le Canada interviendrait ainsi juste après.
Cependant, au-delà de la ratification de l’Accord de Paris, des observateurs alertent sur la [faible ambition des cibles de réduction des émissions de gaz à effet de serre](http://www.lapresse.ca/actualites/politique/politique-canadienne/201609/11/01-5019448-ges-trudeau-sen-tient-aux-cibles-de-harper.php), que le gouvernement canadien a choisi de ne pas réviser.
