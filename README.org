#+title: Paris Agreement: mapping the Paris agreement ratification

A small project to follow the ratification of the Paris Agreement. 

Online at [Paris-Agreement.fr](http://paris-agreement.fr/)

* Things to do
- A script to generate the data from an excel file.

* Data fields

For each country we will display:
- The polygon, from this website: http://unfccc.int/files/inc/application/json/pre2020_mapdata.json or maybe this website http://www.naturalearthdata.com/downloads/110m-cultural-vectors/110m-admin-0 Small country polygons from WRI 
- Ratification procedure `code_ratification` http://scholar.harvard.edu/bsimmons/
- Ratification status `status_ratification`
- Emissions: `emission`, from this table http://unfccc.int/files/ghg_data/application/pdf/table.pdf

* Contributors
- Astrid
- Charles Adrien
- Jay
- Maxime
- Guilhem

* Components

TBC
