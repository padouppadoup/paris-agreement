all: data/data.fr.json data/data.en.json index.html

data/data.fr.json: 
	cd data; python3 convert.py fr > data.fr.json

data/data.en.json:
	cd data; python3 convert.py en > data.en.json

index.html:
	python3 build.py 

clean:
	rm -vf index.html index.fr.html index.en.html table.fr.html table.en.html
	rm -rfv fr/ en/
	rm -vf data/data.*.json
